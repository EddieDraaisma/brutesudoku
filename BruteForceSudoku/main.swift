
//
//  main.swift
//  SudokuSolver
//
//  Created by Eddie Draaisma on 21/12/2018.
//  Copyright © 2018 Draaisma. All rights reserved.
//

import Foundation

func printTimeElapsedWhenRunningCode(_ operation:() -> ()) {
    let startTime = CFAbsoluteTimeGetCurrent()
    operation()
    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
    print("Time elapsed : \(timeElapsed)s")
}

typealias Board = [Int]

let rowCount = 9
let colCount = 9
let posCount = rowCount * colCount


func makeBoard(from gameStr: String) -> Board {
    var board = Array(repeating: 0, count: posCount)
    var pos = 0
    for chr in gameStr {
        let num = Int(String(chr)) ?? 0
        if num != 0 {
            board[pos] = num
        }
        pos += 1
    }
    return board
}


func printBoard(from board: Board) {
    print()
    for row in 0 ..< 9 {
        if [3, 6].contains(row) {
            print(" ------+-------+------")
        }
        print(" ", terminator: "")
        for col in 0 ..< 9 {
            if [3, 6].contains(col) {
                print("| ", terminator: "")
            }
            print(board[row * colCount + col], terminator: " ")
        }
        print()
    }
    print()
}


func validMove(_ board: Board, _ row: Int, _ col: Int, _ num: Int) -> Bool {
    
    for index in 0 ..< 9 {
        if (board[index * 9 + col] == num) || (board[9 * row + index] == num) { return false }
        
        let boxRow = (row / 3) * 3
        let boxCol = (col / 3) * 3
        
        let r1 = boxRow + (row + 1) % 3
        let r2 = boxRow + (row + 2) % 3
        let c1 = boxCol + (col + 1) % 3
        let c2 = boxCol + (col + 2) % 3
        
        if board[r1 * 9 + c1] == num { return false }
        if board[r1 * 9 + c2] == num { return false }
        if board[r2 * 9 + c1] == num { return false }
        if board[r2 * 9 + c2] == num { return false }
    }
    return true
}


func solveBoard(_ board: inout Board, _ pos: Int) -> Board? {
    
    if pos == 81 {
        return board
    }
    if board[pos] != 0 {
        return solveBoard(&board, pos + 1)
    }
    else
    {
        for num in 1 ... 9 {
            let r = pos / 9
            let c = pos % 9
            if validMove(board, r, c, num) {
                board[pos] = num
                if let _ = solveBoard(&board, pos) {
                    return board
                }
                board[pos] = 0
            }
        }
    }

    return nil
}


let grid1 = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
let hard1 = ".....6....59.....82....8....45........3........6..3.54...325..6.................."
let most  = "061007003092003000000000000008530000000000504500008000040000001000160800600000000"
let grid2 = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
let difficult = "800000000003600000070090200050007000000045700000100030001000068008500010090000400"

let diff = "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
let diff2 = "980700000600090800005008090500006080000400300000070002090300700050000010001020004"

let xx = "...8.1..........435............7.8........1...2..3....6......75..34........2..6.."




let game = diff

printTimeElapsedWhenRunningCode {
    var board = makeBoard(from: game)
    if let _ = solveBoard(&board, 0) {
        printBoard(from: board)
    }
}
